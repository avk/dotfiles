-- config file for xmobar

Config { font = "xft:consolas-8:antialias=true"
    , bgColor = "#073642"
    , fgColor = "#93a1a1"
    , position = Bottom
    , commands = [ Run BatteryP ["BAT0"]
                      ["-t", "<acstatus><left>%",
                       "-L", "10", "-H", "80", "-p", "3",
                       "--", "-O", "", "-o", "<fc=#dc322f>B</fc>",
                       "-l", "red", "-m", "blue", "-h", "green"]
                      300
                  , Run Wireless "wlan0" ["-L","0","-H","32","--normal","green","--high","red", "-t", "<essid>"] 10
                  , Run Com "/home/avk/get_volume.sh" [] "volume" 10
                  , Run Date "%a %b %_d %l:%M" "date" 600
                 ]
    , sepChar = "%"
    , alignSep = "}{"
    , template = "}{Vol:%volume% <fc=#859900>%wlan0wi%</fc> %battery% | %date%"
}
