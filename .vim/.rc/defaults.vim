" Sane Defaults {{{
let mapleader=","
set ai                          " set auto-indenting on for programming
set laststatus=2                " make the last line where the status is two lines deep so you can see status always
syntax enable                   " turn syntax highlighting on by default
set noswapfile                  " disable swap files
set ruler                       " show the cursor position all the time
set number                      " show line numbers
set novisualbell
set cindent
set showmode                    " show the current mode
set nocompatible                " vi compatible is LAME
set encoding=utf-8
scriptencoding utf-8
"set autochdir                   " change directory to current file
set list                        " to see tabs and trailing spaces
set listchars=tab:>-,trail:-    " show tabs and trailing
set scrolloff=10                " Keep 10 lines (top/bottom) for scope
set showcmd                     " show the command being typed
set guioptions=ce
com W w                         " Save file even with a Cap W
nnoremap ; :
set wildmenu                    " autocomplete from tab
set wildmode=list:longest       " show file options when opening files
set hidden                      " switch buffers when stale one exists
inoremap jj <esc>
set backspace=indent,eol,start  " delete key work on osx
set clipboard=unnamed           " use system clipboard
set pastetoggle=<F2>            " use F2 to switch to/from toggle mode
set fdm=marker                  " set fold method to marker
set mouse=a                     " enable mouse interaction
set linebreak                   " dont break words
set t_ut=                       " clear background color, for tmux
if (has("termguicolors"))
  set termguicolors            " breaks on ubuntu, urxvt-unicode
endif
"}}}
"indents and spaces {{{
set cindent
set expandtab
set shiftwidth=2
set tabstop=2
set softtabstop=2
"}}}
"search {{{
set ignorecase
set incsearch
set smartcase
set gdefault
set showmatch
set hlsearch
set matchtime=3

nnoremap / /\v
vnoremap / /\v
nnoremap <tab> %
vnoremap <tab> %
"}}}
" To remove highlight after a search "{{{
nnoremap <leader>n :nohl<CR>
vnoremap <leader>n :nohl<CR>
inoremap <leader>n <esc>:nohl<CR>
" }}}
" Cycle through buffers {{{
nmap <leader>l :bnext<CR>
nmap <leader>k :bprevious<CR>
" Show all open buffers and their status
nmap <leader>bl :ls<CR>
"}}}
" Reselect visual block after indentation {{{
vnoremap < <gv
vnoremap > >gv
" }}}
" netrw {{{
let g:netrw_liststyle=3
let g:netrw_browse_split=2
let g:netrw_altv=1
" }}}
" close file keeping the buffer intact {{{
nnoremap <leader>bc :bp\|bd #<CR>
vnoremap <leader>bc :bp\|bd #<CR>
" }}}
