set guifont=Consolas:h9:cANSI
set lines=999 columns=999
set clipboard=unnamed
" make that backspace key work the way it should
set backspace=indent,eol,start

execute 'set viminfo+=n'.home.'/viminfo'

" disable visualbell and audiobell
set noeb vb t_vb=
au GUIEnter * set vb t_vb=

"Windows vim splits
nnoremap <A-k> :wincmd k<CR>
nnoremap <A-j> :wincmd j<CR>
nnoremap <A-h> :wincmd h<CR>
nnoremap <A-l> :wincmd l<CR>

"Open Explorer for the current folder
nnoremap <leader>e :!start explorer .<CR>
