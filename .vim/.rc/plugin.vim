" Load vim-plug if it is not already installed
call plug#begin(plughome)

Plug 'mhinz/vim-startify'
  " :::config/vim-startify {{{
  nnoremap <leader>s :Startify<CR>
  "}}}
Plug 'scrooloose/nerdcommenter'           " Comment all the things
Plug 'scrooloose/nerdtree'                " file viewer
Plug 'EinfachToll/DidYouMean'             " If you misspell a file name
Plug 'scrooloose/syntastic'               " fantastic syntax checker
Plug 'tpope/vim-vinegar'                  " make netrw play better
Plug 'mhinz/vim-signify'                  " git gutter but for many VCS
Plug 'metakirby5/codi.vim'                " codi scratchpad
Plug 'godlygeek/tabular'                  " tabular text, easy
Plug 'easymotion/vim-easymotion'
Plug 'terryma/vim-multiple-cursors'
Plug 'junegunn/goyo.vim'

Plug 'vim-airline/vim-airline'
  " :::config:vim-airline {{{
    "let g:airline_powerline_fonts = 1 " Use powerline fonts for airline
    let g:airline#extensions#tabline#enabled = 1 " Enable the list of buffers
    let g:airline#extensions#tabline#buffer_nr_show = 1 " Enable the buffer numbers
    let g:airline#extensions#tabline#fnamemod = ':t' " Show just the filename
    "let g:airline_theme='lucius'
    " unicode for osx setup, windows machine has unicode issues {{{
    if has("unix")
      if !exists('g:airline_symbols')
      let g:airline_symbols = {}
      endif

      let g:airline_left_sep = '▶'
      let g:airline_right_sep = '◀'
      let g:airline_symbols.linenr = '␊'
      let g:airline_symbols.linenr = '␤'
      let g:airline_symbols.linenr = '¶'
      let g:airline_symbols.branch = '⎇'
      let g:airline_symbols.paste = 'ρ'
      let g:airline_symbols.paste = 'Þ'
      let g:airline_symbols.paste = '∥'
      let g:airline_symbols.whitespace = 'Ξ'
    endif
    " }}}
  "}}}
Plug 'vimwiki/vimwiki'
" :::config:vimwiki{{{
  if has("unix")
    "auto-export to html on vimwiki file save
    let g:vimwiki_list = [{'auto_export': 1}]
  endif
" }}}
Plug 'garbas/vim-snipmate' , { 'for' : 'html' } |
  Plug 'MarcWeber/vim-addon-mw-utils' |
  Plug 'tomtom/tlib_vim' |
  Plug 'honza/vim-snippets'

" colorschemes
Plug 'altercation/vim-colors-solarized'
Plug 'vim-scripts/summerfruit.vim'
Plug 'joshdick/onedark.vim'

" Vim gui only plugins
if has("gui_running")
  Plug 'majutsushi/tagbar'
endif

" osx only Plugins
if has("unix")
  "Plug 'mxw/vim-jsx', { 'for' : 'js,jsx' } "reactNative , has some issues in windows
  " :::config:jsx{{{
    let g:jsx_ext_required = 0 " Allow JSX in normal JS files
  " }}}
  "Plug 'Valloric/YouCompleteMe'

  " vim
  Plug 'sjl/vitality.vim'
  Plug 'christoomey/vim-tmux-navigator'
  Plug 'benmills/vimux'
endif

" Languge Specific
"Plug 'flowtype/vim-flow'
Plug 'Shutnik/jshint2.vim' , { 'for' : 'js' }
Plug 'moll/vim-node' , { 'for' : 'js' }
Plug 'derekwyatt/vim-scala' , { 'for' : 'scala' }
Plug 'avakhov/vim-yaml' , { 'for' : 'yaml' }
Plug 'fatih/vim-go' , { 'for' : 'go' }
Plug 'rodjek/vim-puppet' , { 'for' : 'pp' }
Plug 'pangloss/vim-javascript' , { 'for' : 'js' }
  " vim-javascript {{{
    let g:javascript_plugin_jsdoc           = 1
    let g:javascript_conceal_function       = "ƒ"
    let g:javascript_conceal_null           = "ø"
    let g:javascript_conceal_this           = "@"
    let g:javascript_conceal_return         = "⇚"
    let g:javascript_conceal_undefined      = "¿"
    let g:javascript_conceal_NaN            = "ℕ"
    let g:javascript_conceal_prototype      = "¶"
    let g:javascript_conceal_static         = "•"
    let g:javascript_conceal_super          = "Ω"
  " }}}
Plug 'lambdatoast/elm.vim'
Plug 'aklt/plantuml-syntax'
Plug 'udalov/kotlin-vim'
"Plug 'Valloric/MatchTagAlways'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'bkad/CamelCaseMotion', { 'for' : 'java' }
  " enable camelCaseMotion only for Java and Scala files {{{
    autocmd FileType java,scala map <silent> w <Plug>CamelCaseMotion_w
    autocmd FileType java,scala map <silent> b <Plug>CamelCaseMotion_b
    autocmd FileType java,scala map <silent> e <Plug>CamelCaseMotion_e
    autocmd FileType java,scala map <silent> ge <Plug>CamelCaseMotion_ge
    autocmd FileType java,scala sunmap w
    autocmd FileType java,scala sunmap b
    autocmd FileType java,scala sunmap e
    autocmd FileType java,scala sunmap ge
  " }}}

call plug#end()
