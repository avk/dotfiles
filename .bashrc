# Check for an interactive session
[ -z "$PS1" ] && return

PATH="/usr/local/bin:$PATH"
export HISTSIZE=10000
export HISTCONTROL=ignoreboth:erasedups # don't save duplicate bash history
force_color_prompt=yes                  # shell prompt
export CLICOLOR=1                       # ls colors
shopt -s checkwinsize                   # check the window size after each command and, if necessary, update the values of LINES and COLUMNS.
shopt -s cdspell                        # correct minor spelling errors in cd
shopt -s dotglob                        # include dotfiles in wildcard expansion, and match case-insensitively
shopt -s nocaseglob
#set -o vi                              # use vim mode in bash

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

# aliases
alias vi='vim'
alias serve='python -m SimpleHTTPServer'
alias e.='open .'
alias balance="hledger balance -N --depth 1"
alias lsusb="system_profiler SPUSBDataType"
alias bs="brew services"
alias cd..='cd ..'
alias ..='cd ..'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias dc='docker-compose'

# git config
git config --global core.editor "vim"
git config --global color.ui true
git config --global push.default simple

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1='\[\033[01;32m\]\w\[\033[00m\]$(parse_git_branch) '

# Hook for desk activation
[ -n "$DESK_ENV" ] && source "$DESK_ENV" || true

source ~/.dotfiles/bin/bashmarks.sh # setup bashmarks

[ -f ~/.fzf.bash ] && source ~/.fzf.bash


# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi
