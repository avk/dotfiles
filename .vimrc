if has("unix")
  let home='~'
  let plughome='~/.vim/plugged'
elseif has("win32")
  let plughome='C:\dev\cmder\vendor\msysgit\share\vim\vim74\plugged'
  let home='$VIMRUNTIME'
endif

source ~/.vim/.rc/defaults.vim
source ~/.vim/.rc/plugin.vim
if has("win32")
  source ~/.vim/.rc/win.vim
endif

colorscheme mustang

" Adding filetypes for coloring {{{
au BufNewFile,BufRead *.jsm set filetype=javascript
au BufNewFile,BufRead *.handlebars set filetype=html
au BufNewFile,BufRead *.dump set filetype=sql
au BufNewFile,BufRead *.install set filetype=php
au BufRead,BufNewFile *.tpl set filetype=smarty
au BufNewFile,BufRead *.less set filetype=less
au BufNewFile,BufRead *.rs set filetype=rust
au BufNewFile,BufRead *.gradle set filetype=groovy
" }}}
" fzf {{{
nnoremap <C-P> :FZF<CR>
vnoremap <C-P> :FZF<CR>
inoremap <C-P> <esc>:FZF<CR>
" }}}
set autochdir
if filereadable("~/.vim/.rc/local.vim")
  source ~/.vim/.rc/local.vim
endif
