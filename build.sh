#!/bin/bash

mkdir -p ./.vim/autoload
if [ ! -f ./.vim/autoload/plug.vim ]
then
  echo "Downloading vimPlug"
  wget -O ./.vim/autoload/plug.vim https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

home_links=(
".vimrc"
".vim"
".bashrc"
".gitconfig"
".gitmodules"
".tmux.conf"
".urxvt"
".Xresources"
)

for i in ${home_links[@]}; do
  echo "linking $i"
  if [ -L ~/$i ]; then rm ~/$i; fi # remove previous symlinks
  mv ~/$i ~/$i-old-$(date +"%m-%d-%y-%HH-%MM-%SS") 2>/dev/null # move old files and keep as backup .bashrc -> .bashrc-old-<date>
  ln -s $(readlink -f $i) ~/$i # symlink new ones
done

echo "running vimPlug install"
vim +PluginInstall +qall
