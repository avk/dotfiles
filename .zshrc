# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="robbyrussell"
#ZSH_THEME="miloshadzic"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
#plugins=(git svn-fast-info)

source $ZSH/oh-my-zsh.sh
unsetopt correct_all

# Customize to your needs...
#export PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/mysql/bin:/usr/local/lib:/Android/android-sdk-macosx/tools:/Android/android-sdk-macosx/platform-tools:/Users/avk/.rbenv/shims:/Users/avk/.rbenv/bin:$PATH

#so that opencv bindings are available
#export PYTHONPATH="/usr/local/lib/python2.7/site-packages/:$PYTHONPATH"

#export NODE_PATH=/usr/local/lib/jsctags/:$NODE_PATH
#alias sendmail="open -a Thunderbird"
#alias deployjekyll="git update-ref refs/heads/master $(echo 'push _site' | git commit-tree source^{tree}:_site -p $(cat .git/refs/heads/master))"
#

#PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

#export GEM_HOME=$HOME/gems
#export PATH=$GEM_HOME/bin:$PATH

## The next line updates PATH for the Google Cloud SDK.
#export PATH=/Users/avk/Programs/google-sdk/google-cloud-sdk/bin:$PATH

# The next line enables bash completion for gcloud.
#source /Users/avk/Programs/google-sdk/google-cloud-sdk/arg_rc

# colorful man pages
export PAGER=less
export LESS_TERMCAP_mb=$'\E[01;33m' 
export LESS_TERMCAP_md=$'\E[01;31m' 
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;42;30m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'
export LESS_TERMCAP_ue=$'\E[0m'

# ctrl-z to get back to suspended process
fancy-ctrl-z () {
  if [[ $#BUFFER -eq 0 ]]; then
    BUFFER="fg"
    zle accept-line
  else
    zle push-input
    zle clear-screen
  fi
}
zle -N fancy-ctrl-z
bindkey '^Z' fancy-ctrl-z

# XFCE specific bindings
if [ "$XDG_CURRENT_DESKTOP" = "XFCE" ]
then
  #use open like in osx
  alias open='exo-open'

  alias tmux='nocorrect tmux -2'
  alias checkDrive='sudo fsck.hfsplus -f /dev/sda3'

  #to be able to use rubygems witout sudo
  export GEM_HOME=$HOME/gems
  export PATH=$GEM_HOME/bin:$PATH
fi


#crazy dirty hack to see if i am using osx 
if [ "$HOME" = "/Users/avk" ]
then
  #autcompletion fpath
  fpath=($HOME/.zsh/completions $fpath)
  # enable autocomplete function
  autoload -U compinit
  compinit

  alias vi='vim'
  alias v='vim'
  alias simpleServer='python -m SimpleHTTPServer'

  unalias l
  source ~/bashmarks.sh

  export PATH=/usr/local/sbin:$PATH:/Users/avk/bin
  export SVN_EDITOR=vim
  export PATH=/Applications/Android\ Studio.app/sdk/platform-tools:$PATH

  #configure / set rbenv
  eval "$(rbenv init -)"

  export GOPATH=$HOME/Programs/go
  export PATH=$PATH:$GOPATH/bin

  #php-version
  source $(brew --prefix php-version)/php-version.sh && php-version 5

fi
